import React from 'react';
import {View, Text, Alert, StyleSheet, TextInput} from 'react-native';
import {CheckBox, Button} from 'react-native-elements';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: true,
      arrayNumber: [],
      result: 0,
      number1: 0,
      number2: 0,
      number3: 0,
      numberCheck1: false,
      numberCheck2: false,
      numberCheck3: false,
      buttonArray: ['+', '-', 'x', '/'],
    };
  }

  calculator(type) {
    this.setState({result: 0});
    const {
      number1,
      number2,
      number3,
      numberCheck1,
      numberCheck2,
      numberCheck3,
    } = this.state;
    var number = [];
    if (numberCheck1) number.push(number1);
    if (numberCheck2) number.push(number2);
    if (numberCheck3) number.push(number3);

    if (number.length < 2) {
      Alert.alert(
        'Error',
        'Operasi harus lebih dari 1 digit, silahkan ceklis nomor lainnya',
      );
      return false;
    }

    var i;
    var result = number[0];
    for (i = 0; i < number.length; i++) {
      if (i > 0) {
        if (type == '/') {
          result = parseFloat(result) / parseFloat(number[i]);
        } else if (type == '+') {
          result = parseFloat(result) + parseFloat(number[i]);
        } else if (type == '-') {
          result = parseFloat(result) - parseFloat(number[i]);
        } else {
          result = parseFloat(result) * parseFloat(number[i]);
        }
      }
    }
    if (result % 1 === 0) {
      this.setState({result});
    } else {
      if (isNaN(result)) {
        this.setState({result: 0});
      } else {
        this.setState({result: result.toFixed(2)});
      }
    }
  }

  render() {
    const {
      checked,
      value,
      result,
      number1,
      number2,
      number3,
      numberCheck1,
      numberCheck2,
      numberCheck3,
      buttonArray,
    } = this.state;
    return (
      <View style={styles.container}>
        {/* number1 */}
        <Text style={{fontSize: 20}}>Calculator Sederhana</Text>
        <View style={styles.inputCont}>
          <View style={{width: '80%'}}>
            <TextInput
              keyboardType="numeric"
              style={{width: '100%', borderColor: 'gray', borderWidth: 1}}
              onChangeText={(number1) => this.setState({number1})}
              value={number1}
            />
          </View>
          <View style={{width: '20%'}}>
            <CheckBox
              title=""
              checked={numberCheck1}
              onPress={() => this.setState({numberCheck1: !numberCheck1})}
            />
          </View>
        </View>

        {/* number2 */}
        <View style={styles.inputCont}>
          <View style={{width: '80%'}}>
            <TextInput
              keyboardType="numeric"
              style={{width: '100%', borderColor: 'gray', borderWidth: 1}}
              onChangeText={(number2) => this.setState({number2})}
              value={number2}
            />
          </View>
          <View style={{width: '20%'}}>
            <CheckBox
              title=""
              checked={numberCheck2}
              onPress={() => this.setState({numberCheck2: !numberCheck2})}
            />
          </View>
        </View>

        {/* number3 */}
        <View style={styles.inputCont}>
          <View style={{width: '80%'}}>
            <TextInput
              keyboardType="numeric"
              style={{width: '100%', borderColor: 'gray', borderWidth: 1}}
              onChangeText={(number3) => this.setState({number3})}
              value={number3}
            />
          </View>
          <View style={{width: '20%'}}>
            <CheckBox
              title=""
              checked={numberCheck3}
              onPress={() => this.setState({numberCheck3: !numberCheck3})}
            />
          </View>
        </View>
        <View style={styles.btnCont}>
          {buttonArray.map((r, i) => {
            return (
              <Button
                onPress={() => {
                  this.calculator(r);
                }}
                key={i}
                buttonStyle={styles.btnStyle}
                title={r}
              />
            );
          })}
        </View>
        <View style={styles.hr} />
        <View style={styles.resultCont}>
          <Text style={{fontSize: 30}}>Hasil :</Text>
          <Text style={{fontSize: 30}}>{result}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    margin: 10,
  },
  inputCont: {
    marginBottom: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnCont: {
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '80%',
  },
  btnStyle: {
    width: 40,
  },
  hr: {
    borderBottomColor: 'black',
    borderBottomWidth: 1,
    width: '80%',
    marginTop: 10,
  },
  resultCont: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '80%',
  },
});

export default App;
